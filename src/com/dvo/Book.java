package com.dvo;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Danyl on 028 28.07.17.
 */
public class Book {
    private List<BookEntry> myBook;
    Book(List<BookEntry> myBook){
        this.myBook = myBook;
    }

    public void printSorted() {
        for (BookEntry temp:myBook) {
            System.out.println(temp.getFirstName() + " " + temp.getSecondName() + " " + temp.getPhoneNumber());
        }
        System.out.println();
    }

    public List sortFields(Method method) {
        switch(method){
            case SORT_BY_NAME:
                 this.myBook.sort(new Comparator<BookEntry>() {
                     @Override
                     public int compare(BookEntry o1, BookEntry o2) {
                         return o1.getFirstName().compareTo(o2.getFirstName());
                     }
                 });
                break;
            case SORT_BY_SECOND_NAME:
                this.myBook.sort(new Comparator<BookEntry>() {
                    @Override
                    public int compare(BookEntry o1, BookEntry o2) {
                        return o1.getSecondName().compareTo(o2.getSecondName());
                    }
                });
                break;
            case SORT_BY_PHONE_NUMBER:
                this.myBook.sort(new Comparator<BookEntry>() {
                    @Override
                    public int compare(BookEntry o1, BookEntry o2) {
                        return o1.getPhoneNumber().compareTo(o2.getPhoneNumber());
                    }
                });
                break;
            case SORT_BY_FIRST_AND_SECOND_NAME:
                this.myBook.sort(new Comparator<BookEntry>() {
                    @Override
                    public int compare(BookEntry o1, BookEntry o2) {
                        if(o1.getFirstName().equals(o2.getFirstName())){
                            return o1.getSecondName().compareTo(o2.getSecondName());
                        }else{
                            return o1.getFirstName().compareTo(o2.getFirstName());
                        }
                    }
                });
                break;
        }
        return null;
    }
}
