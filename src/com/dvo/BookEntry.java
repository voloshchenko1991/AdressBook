package com.dvo;

/**
 * Created by Danyl on 028 28.07.17.
 */
public class BookEntry {
    private String firstName;
    private String secondName;
    private String phoneNumber;

    BookEntry(String firstName, String secondName, String phoneNumber){
        this.firstName = firstName;
        this.secondName = secondName;
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getSecondName() {
        return secondName;
    }

}
