package com.dvo;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int inputMethod = 0;

        List<BookEntry> bookEntries = new ArrayList<>();
        bookEntries.add(new BookEntry("Max", "Leshev", "+380506724589"));
        bookEntries.add(new BookEntry("Oleg", "Oprishko", "+380633215799"));
        bookEntries.add(new BookEntry("Olga", "Kononenko", "+380952245747"));
        bookEntries.add(new BookEntry("Svetlana", "Koltsova", "+380503681579"));
        bookEntries.add(new BookEntry("Mihail", "Derevjanchuk", "+380996571227"));
        bookEntries.add(new BookEntry("Viktoriya", "Peretokina", "+380632235710"));

        Book book = new Book(bookEntries);

        while(inputMethod != 4) {
            System.out.println("Enter sorting method:\n0 - Sort by name");
            System.out.println("1 - Sort by second name");
            System.out.println("2 - Sort by phone number");
            System.out.println("3 - Sort by first and second name");
            System.out.println("4 - To exit");
            System.out.println();
            inputMethod = scanner.nextInt();
            if(inputMethod < 4 && inputMethod >= 0) {

                book.sortFields(Method.values()[inputMethod]);
                book.printSorted();
            }else if(inputMethod == 4){
                break;
            }else{
                System.out.println("Invalid number!");
            }
        }
    }
}
