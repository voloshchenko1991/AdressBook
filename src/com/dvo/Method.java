package com.dvo;

/**
 * Created by Danyl on 028 28.07.17.
 */
public enum Method {
    SORT_BY_NAME,
    SORT_BY_SECOND_NAME,
    SORT_BY_PHONE_NUMBER,
    SORT_BY_FIRST_AND_SECOND_NAME
}
